<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate([
            "first_name" =>  "Đỗ Tiến",
            "last_name" =>  "Anh",
            "name" =>  "TienDT",
            'email' => 'oragen2000@gmail.com',
            "role" =>  'admin',
            "avatar" =>  'https://webneel.com/daily/sites/default/files/images/daily/08-2018/1-nature-photography-spring-season-mumtazshamsee.jpg',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'phone_number' => '0963328520',
            'remember_token' => Str::random(10),
        ]);
    }
}
