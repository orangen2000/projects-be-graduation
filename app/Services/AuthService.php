<?php

namespace App\Services;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserResource;
use App\Repositories\Profile\ProfileRepositoryInterface;

class AuthService
{
    private $profileRepository;

    public function __construct(ProfileRepositoryInterface $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    public function login($request, $roleLogin = null)
    {
        $credentials = $request->only(['email', 'password']);
        $user = $this->profileRepository->first(['email' => $credentials['email']]);

        if (!$user) {
            return [
                'success' => false,
                'data' => [
                    'fieldName' => 'email',
                ]
            ];
        }

        if (!Hash::check($credentials['password'], $user->password)) {
            return [
                'success' => false,
                'data' => [
                    'fieldName' => 'password',
                ]
            ];
        }

        if ($roleLogin && $user->role !== $roleLogin) {
            return [
                'success' => false,
                'data' => [
                    'fieldName' => 'role',
                ]
            ];
        }

        $tokenResult = $user->createToken('Personal Access Token');

        return [
            'success' => true,
            'data' => [
                "user" => new UserResource($user),
                "access_token" => $tokenResult->accessToken ?? '',
                "token_type" => "Bearer",
                "expires_at" => Carbon::parse($tokenResult->expires_in ?? '')->toDateTimeString(),
            ]
        ];
    }
}
