<?php

namespace App\Services;

use App\Repositories\Profile\ProfileRepositoryInterface;

class ProfileService
{
    private $profileRepository;

    public function __construct(
        ProfileRepositoryInterface $profileRepository
    ) {
        $this->profileRepository = $profileRepository;
    }

    public function searchProfile($request)
    {
        return $this->profileRepository->searchProfile($request);
    }

    public function listProfile()
    {
        return $this->profileRepository->listProfile();
    }
}
