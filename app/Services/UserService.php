<?php

namespace App\Services;

use App\Repositories\User\UserRepositoryInterface;

class UserService
{
    private $UserRepository;

    public function __construct(
        UserRepositoryInterface $UserRepository
    ) {
        $this->UserRepository = $UserRepository;
    }

    public function searchUser($request)
    {
        return $this->UserRepository->searchUser($request);
    }

    public function detailUser($id)
    {
        return $this->UserRepository->detailUser($id);
    }

    public function listUser()
    {
        return $this->UserRepository->listUser();
    }

    public function createUser($data)
    {
        return $this->UserRepository->createUser($data);
    }

    public function updateUser($id, $data)
    {
        return $this->UserRepository->updateUser($id, $data);
    }

    public function deleteUser($id)
    {
        return $this->UserRepository->deleteUser($id);
    }
}
