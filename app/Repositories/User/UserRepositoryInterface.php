<?php

namespace App\Repositories\User;

use App\Repositories\BaseRepositoryInterface;

interface UserRepositoryInterface extends BaseRepositoryInterface
{
    public function searchUser($request);
    public function detailUser($id);
    public function listUser();
    public function createUser($data);
    public function updateUser($id, $data);
    public function deleteUser($id);
}
