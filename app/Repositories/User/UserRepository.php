<?php

namespace App\Repositories\User;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\DatabaseManager;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    protected $model;

    private $db;

    public function __construct(User $model, DatabaseManager $db)
    {
        parent::__construct($model);

        $this->db = $db;
    }

    public function searchUser($request)
    {
        return $this->model->where('id', $request->id)->get();
    }

    public function detailUser($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function listUser()
    {
        return $this->model->where('delete', '1')->orderBy('id', 'DESC');
    }

    public function createUser($data)
    {
        return $this->model::create($data);
    }

    public function updateUser($id, $data)
    {
        $this->model->where('id', $id)->update($data);

        return $this->model->where('id', $id)->first();
    }

    public function deleteUser($id)
    {
        $this->model->where('id', $id)->update(["delete" => "0"]);

        return $this->model->where('id', $id)->first();
    }
}
