<?php

namespace App\Repositories\Profile;

use App\Repositories\BaseRepositoryInterface;

interface ProfileRepositoryInterface extends BaseRepositoryInterface
{
    public function searchProfile($request);
    public function listProfile();
}
