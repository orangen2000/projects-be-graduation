<?php

namespace App\Repositories\Profile;

use App\Models\User;
use App\Repositories\BaseRepository;
use Illuminate\Database\DatabaseManager;

class ProfileRepository extends BaseRepository implements ProfileRepositoryInterface
{
    protected $model;

    private $db;

    public function __construct(User $model, DatabaseManager $db)
    {
        parent::__construct($model);

        $this->db = $db;
    }

    public function searchProfile($request)
    {
        return $this->model->where('id', $request->id)->get();
    }

    public function listProfile()
    {
        return $this->model->orderBy('id', 'DESC');
    }
}
