<?php

namespace App\Repositories\Product;

use App\Models\Product;
use App\Repositories\BaseRepository;
use Illuminate\Database\DatabaseManager;

class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{
    protected $model;

    private $db;

    public function __construct(Product $model, DatabaseManager $db)
    {
        parent::__construct($model);

        $this->db = $db;
    }

    public function searchProduct($request)
    {
        return $this->model->where('name', $request->name)->get();
    }

    public function detailProduct($id)
    {
        return $this->model->where('id', $id)->first();
    }

    public function listProduct()
    {
        return $this->model->where('delete', '1')->orderBy('id', 'DESC');
    }

    public function createProduct($data)
    {
        return $this->model::create($data);
    }

    public function updateProduct($id, $data)
    {
        $this->model->where('id', $id)->update($data);

        return $this->model->where('id', $id)->first();
    }

    public function deleteProduct($id)
    {
        $this->model->where('id', $id)->update(["delete" => "0"]);

        return $this->model->where('id', $id)->first();
    }
}
