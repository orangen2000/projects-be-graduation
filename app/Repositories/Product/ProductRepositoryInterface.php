<?php

namespace App\Repositories\Product;

use App\Repositories\BaseRepositoryInterface;

interface ProductRepositoryInterface extends BaseRepositoryInterface
{
    public function searchProduct($request);
    public function detailProduct($id);
    public function listProduct();
    public function createProduct($data);
    public function updateProduct($id, $data);
    public function deleteProduct($id);
}
