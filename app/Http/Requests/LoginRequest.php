<?php

namespace App\Http\Requests;

use App\Enums\MessageEnum;
use App\Http\Traits\FailedValidation;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    use FailedValidation;

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'required|string|max:256',
            'password' => 'required|string',
            'token_device' => 'nullable',
        ];
    }

    public function messages(): array
    {
        return [
            'required' => MessageEnum::MSG_03,
        ];
    }
}
