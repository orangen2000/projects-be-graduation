<?php

namespace App\Http\Controllers\User;

use App\Enums\MessageEnum;
use Illuminate\Http\Request;
use App\Services\ProfileService;
use App\Http\Resources\ProfileResource;

class ProfileController extends BaseController
{
    protected $profileService;

    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    public function search(Request $request)
    {
        $searchProfile = $this->profileService->searchProfile($request);

        return $this->responseSuccess($searchProfile, MessageEnum::MSG_06);
    }

    public function list(Request $request)
    {
        $listProfile = ProfileResource::apiPaginate($this->profileService->listProfile(), $request);

        return $this->responseSuccess($listProfile, MessageEnum::MSG_06);
    }
}
