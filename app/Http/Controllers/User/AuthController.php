<?php

namespace App\Http\Controllers\User;

use App\Enums\RoleEnum;
use App\Enums\MessageEnum;
use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function login(LoginRequest $request)
    {
        $login = $this->authService->login($request, RoleEnum::USER);

        if (!$login['success']) {
            return $this->responseError($login['data'], MessageEnum::MSG_01);
        }

        return $this->responseSuccess($login['data'], MessageEnum::MSG_02);
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();

        return $this->responseSuccess([], MessageEnum::MSG_02);
    }
}
