<?php

namespace App\Http\Controllers\Admin;

use App\Enums\RoleEnum;
use App\Enums\MessageEnum;
use Illuminate\Http\Request;
use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends BaseController
{

    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function login(LoginRequest $request)
    {
        $login = $this->authService->login($request, RoleEnum::ADMIN);

        if (!$login['success']) {
            return $this->responseError($login['data'], MessageEnum::MSG_01);
        }

        return $this->responseSuccess($login['data'], MessageEnum::MSG_02);
    }

    public function changePassword(Request $request)
    {
        $user = $request->user();
        if (!(Hash::check($request->input('old_password'), $user->password))) {
            return $this->responseError([], MessageEnum::MSG_07, Response::HTTP_PAYMENT_REQUIRED,);
        }

        if (Hash::check($request->input('password'), $user->password)) {
            return $this->responseError([], MessageEnum::MSG_08, Response::HTTP_FORBIDDEN,);
        }

        $user->update([
            'password' => Hash::make($request->input('confirm_password'))
        ]);

        return $this->responseSuccess([], MessageEnum::MSG_02);
    }

    public function logout()
    {
        Auth::user()->tokens()->delete();

        return $this->responseSuccess([], MessageEnum::MSG_02);
    }
}
