<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MessageEnum;
use Illuminate\Http\Request;
use App\Services\ProductService;
use App\Http\Resources\ProductResource;

class ProductController extends BaseController
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function search(Request $request)
    {
        $searchProduct = ProductResource::collection($this->productService->searchProduct($request));

        return $this->responseSuccess($searchProduct, MessageEnum::MSG_06);
    }

    public function detail(Request $request)
    {
        $detailProduct = new ProductResource($this->productService->detailProduct($request->id));

        return $this->responseSuccess($detailProduct, MessageEnum::MSG_06);
    }

    public function list(Request $request)
    {
        $listProduct = ProductResource::apiPaginate($this->productService->listProduct(), $request);

        return $this->responseSuccess($listProduct, MessageEnum::MSG_06);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->only([
            "name",
            "price",
            "like",
            "image",
        ]);
        $dataCreate['delete'] = "1";
        $createProduct = new ProductResource($this->productService->createProduct($dataCreate));

        return $this->responseSuccess($createProduct, MessageEnum::MSG_06);
    }

    public function update(Request $request)
    {
        $dataEdit = $request->only([
            "name",
            "price",
            "like",
            "image",
        ]);
        $updateProduct = new ProductResource($this->productService->updateProduct($request->id, $dataEdit));

        return $this->responseSuccess($updateProduct, MessageEnum::MSG_06);
    }

    public function delete(Request $request)
    {
        $deleteProduct = new ProductResource($this->productService->deleteProduct($request->id));

        return $this->responseSuccess($deleteProduct, MessageEnum::MSG_06);
    }
}
