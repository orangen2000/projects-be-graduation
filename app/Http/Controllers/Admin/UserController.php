<?php

namespace App\Http\Controllers\Admin;

use App\Enums\MessageEnum;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Resources\UserResource;

class UserController extends BaseController
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function search(Request $request)
    {
        $searchUser = UserResource::collection($this->userService->searchUser($request), $request);

        return $this->responseSuccess($searchUser, MessageEnum::MSG_06);
    }

    public function detail(Request $request)
    {
        $detailUser = new UserResource($this->userService->detailUser($request->id));

        return $this->responseSuccess($detailUser, MessageEnum::MSG_06);
    }

    public function list(Request $request)
    {
        $listUser = UserResource::apiPaginate($this->userService->listUser(), $request);

        return $this->responseSuccess($listUser, MessageEnum::MSG_06);
    }

    public function create(Request $request)
    {
        $dataCreate = $request->only([
            "name",
            "email",
            "phone_number",
            "role",
            "avatar",
        ]);
        $dataCreate['delete'] = "1";
        $dataCreate['password'] = bcrypt($request->password);
        $createUser = new UserResource($this->userService->createUser($dataCreate));

        return $this->responseSuccess($createUser, MessageEnum::MSG_06);
    }

    public function update(Request $request)
    {
        $dataEdit = $request->only([
            "name",
            "email",
            "phone_number",
            "role",
            "avatar",
        ]);
        $updateUser = new UserResource($this->userService->updateUser($request->id, $dataEdit));

        return $this->responseSuccess($updateUser, MessageEnum::MSG_06);
    }

    public function delete(Request $request)
    {
        $deleteUser = new UserResource($this->userService->deleteUser($request->id));

        return $this->responseSuccess($deleteUser, MessageEnum::MSG_06);
    }
}
