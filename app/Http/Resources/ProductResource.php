<?php

namespace App\Http\Resources;

class ProductResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'name' => $this->name ?? null,
            'price' => $this->price ?? null,
            'like' => $this->like ?? null,
            'image' => $this->image ?? null,
            'delete' => $this->delete ?? null,
            'created_at' => $this->created_at ?? null,
        ];
    }
}
