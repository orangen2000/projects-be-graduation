<?php

namespace App\Http\Resources;

class UserResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'first_name' => $this->first_name ?? null,
            'last_name' => $this->last_name ?? null,
            'name' => $this->name ?? null,
            'role' => $this->role ?? null,
            'email' => $this->email ?? null,
            'password' => $this->password ?? null,
            'avatar' => $this->avatar ?? null,
            'phone_number' => $this->phone_number ?? null,
            'delete' => $this->delete ?? null,
        ];
    }
}
