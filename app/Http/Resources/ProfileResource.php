<?php

namespace App\Http\Resources;

class ProfileResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'first_name' => $this->first_name ?? null,
            'last_name' => $this->last_name ?? null,
            'name' => $this->name ?? null,
            'role' => $this->role ?? null,
            'email' => $this->email ?? null,
            'avatar' => $this->avatar ?? null,
            'phone_number' => $this->phone_number ?? null,
        ];
    }
}
