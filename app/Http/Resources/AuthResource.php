<?php

namespace App\Http\Resources;

class AuthResource extends BaseResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'role' => $this->role ?? null,
            'name' => $this->name ?? null,
            'email' => $this->email ?? null,
            'avatar' => $this->avatar ?? null,
        ];
    }
}
