<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class RoleEnum extends Enum
{
    const USER = 'user';
    const ADMIN = 'admin';
}
