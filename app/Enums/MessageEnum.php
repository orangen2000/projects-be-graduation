<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class MessageEnum extends Enum
{
    const MSG_01 = 'Login account user failed';
    const MSG_02 = 'Login account user successfully';
    const MSG_03 = 'Information cannot be left blank';
    const MSG_04 = 'Information is too short';
    const MSG_05 = 'Information is too long';
    const MSG_06 = 'Successfully!!';
    const MSG_07 = 'The old password is incorrect';
    const MSG_08 = 'The new password must not be the same as the old password';
}
