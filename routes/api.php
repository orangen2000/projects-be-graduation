<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
], function () {
    Route::controller(App\Http\Controllers\Admin\AuthController::class)->group(function () {
        Route::post('register', 'register');
        Route::post('login', 'login');
        Route::group([
            'middleware' => ['auth:api']
        ], function () {
            // Authentication
            Route::post('logout', 'logout');
            Route::post('change-password', 'changePassword');

            Route::group([
                'prefix' => 'product',
            ], function () {
                // Product
                Route::controller(App\Http\Controllers\Admin\ProductController::class)->group(function () {
                    Route::get('search', 'search');
                    Route::get('detail', 'detail');
                    Route::get('list', 'list');
                    Route::post('create', 'create');
                    Route::post('update/{id}', 'update');
                    Route::delete('delete/{id}', 'delete');
                });
            });

            Route::group([
                'prefix' => 'user',
            ], function () {
                // User
                Route::controller(App\Http\Controllers\Admin\UserController::class)->group(function () {
                    Route::get('search', 'search');
                    Route::get('detail', 'detail');
                    Route::get('list', 'list');
                    Route::post('create', 'create');
                    Route::post('update/{id}', 'update');
                    Route::delete('delete/{id}', 'delete');
                });
            });
        });
    });
});

Route::controller(App\Http\Controllers\User\AuthController::class)->group(function () {
    Route::post('register', 'register');
    Route::post('login', 'login');
});

Route::group([
    'middleware' => ['auth:api']
], function () {
    Route::controller(App\Http\Controllers\User\AuthController::class)->group(function () {
        // Authentication
        Route::post('logout', 'logout');

        Route::group([
            'prefix' => 'profile',
        ], function () {
            Route::controller(App\Http\Controllers\User\ProfileController::class)->group(function () {
                Route::get('search', 'search');
                Route::get('list', 'list');
            });
        });
    });
});
