<?php

return [
    'app_url' => env('APP_URL'),
    'pagination' => [
        'per_page' => 10,
        'max_per_page' => 20,
    ],
];
